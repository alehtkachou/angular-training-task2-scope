"use strict";

function Scope() {
    this.__watchers = [];
    this.__asyncQueue = [];
    this.__phase = null;
    this.__postDigestQueue = [];
};

Scope.prototype.$watch = function (watchFn, listenerFn, valueEq) {

    var self = this;

    var watcher = {
        watchFn: watchFn,
        listenerFn: listenerFn || function () {},
        valueEq: !!valueEq
    };

    self.__watchers.push(watcher);

    return function () {
        var index = self.__watchers.indexOf(watcher);

        if (index >= 0) {
            self.__watchers.splice(index, 1);
        }
    };
};

Scope.prototype.__digestOnce = function () {
    var self  = this;
    var dirty;

    _.forEach(this.__watchers, function (watch) {
        try {
            var newValue = watch.watchFn(self);
            var oldValue = watch.last;

            if (!self.__areEqual(newValue, oldValue, watch.valueEq)) {
                watch.listenerFn(newValue, oldValue, self);
                dirty = true;
            }

            watch.last = (watch.valueEq ? _.cloneDeep(newValue) : newValue);
        } catch (error) {
            console.error(error);
        }
    });
    return dirty;
};

Scope.prototype.$digest = function () {
    var timeToLive = 11;
    var dirty;

    this.$beginPhase("$digest");

    do {
        while (this.__asyncQueue.length) {
            try {
                var asyncTask = this.__asyncQueue.shift();

                this.$eval(asyncTask.expression);
            } catch (error) {
                console.error(error);
            }
        }

        dirty = this.__digestOnce();

        if (dirty && !(timeToLive--)) {
            this.$clearPhase();

            throw "11 digest iterations is done!";
        }
    } while (dirty);

    this.$clearPhase();

    while (this.__postDigestQueue.length) {
        try {
            this.__postDigestQueue.shift()();
        } catch (error) {
            console.error(error);
        }
    }
};

Scope.prototype.__areEqual = function (newValue, oldValue, valueEq) {
    if (valueEq) {
        return _.isEqual(newValue, oldValue);
    } else {
            return newValue === oldValue || (typeof newValue === 'number' && typeof oldValue === 'number' && isNaN(newValue) && isNaN(oldValue));
        }
};

Scope.prototype.$eval = function (expr, locals) {
    return expr(this, locals);
};

Scope.prototype.$apply = function (expr) {
    try {
        this.$beginPhase("$apply");

        return this.$eval(expr);
    } finally {
        this.$clearPhase();
        this.$digest();
    }
};

Scope.prototype.$evalAsync = function (expr) {
    var self = this;

    if (!self.__asyncQueue.length && !self.__phase) {
        setTimeout(function () {
            if (self.__asyncQueue.length) {
                self.$digest();
            }
        }, 0);
    }

    self.__asyncQueue.push({scope: self, expression: expr});
};

Scope.prototype.$beginPhase = function (phase) {
    if (this.__phase) {
        throw this.__phase + " in progress";
    }

    this.__phase = phase;
};

Scope.prototype.$clearPhase = function () {
    this.__phase = null;
};

Scope.prototype.__postDigest = function (func) {
    this.__postDigestQueue.push(func);
};

Scope.prototype.$watchGroup = function (watchFns, listenerFn) {
    var self = this;

    var oldValues = new Array(watchFns.length);
    var newValues = new Array(watchFns.length);

    var changeReactionScheduled = false;
    var firstRun = true;

    if (watchFns.length === 0) {
        var shouldCall = true;

        self.$evalAsync(function () {
            if (shouldCall) {
                listenerFn(newValues, newValues, self);
            }
        });

        return function () {
            shouldCall = false;
        };
    }

    function watchGroupListener () {
        if (firstRun) {
            firstRun = false;

            listenerFn(newValues, newValues, self);
        } else {
            listenerFn(newValues, oldValues, self);
        }

        changeReactionScheduled = false;
    }

    var destroyFunctions = _.map(watchFns, function (watchFn, index) {
        return self.$watch(watchFn, function (newValue, oldValue) {
            newValues[index] = newValue;
            oldValues[index] = oldValue;

            if (!changeReactionScheduled) {
                changeReactionScheduled = true;
                self.$evalAsync(watchGroupListener);
            }
        });
    });

    return function () {
        _.forEach(destroyFunctions, function (destroyFunction) {
            destroyFunction();
        });
    };
};


var scopeF = new Scope();

scopeF.counter = 0;

scopeF.asyncEv = false;


var postDigestInv = false;


var destroyGroup = scopeF.$watchGroup([
    function(scope) {
        return scope.nameValue;
    },
    function(scope) {
        return scope.ageValue;
    },
    function(scope) {
        return scope.yearValue;
    }],

    function(newValue, oldValue, scope) {
        scope.counter++;

        scope.$evalAsync(function (scope) {
            scope.asyncEv = true;
        });

        scope.__postDigest(function () {
            postDigestInv = true;
        });

        console.log(newValue);
    }
);

console.log("postDigestInv before apply: " + postDigestInv);

scopeF.$apply(function (scope) {
    scope.nameValue = "Alex";
    scope.ageValue = 18;
    scope.yearValue = 2015;
});

console.log("Counter: " + scopeF.counter); // 1
console.log("asyncEv after digest: "+ scopeF.asyncEv);
console.log("postDigestInv: " + postDigestInv);


scopeF.$apply(function (scope) {
    scope.nameValue = 'Alex';
    scope.ageValue = 18;
    scope.yearValue = 2016;
});

console.log("Counter: " + scopeF.counter); // 2


destroyGroup();

scopeF.$apply(function (scope) {
    scope.nameValue = 'Jim';
    scope.ageValue = 20;
    scope.yearValue = 2018;
});

console.log("Counter: " + scopeF.counter); // 2

setTimeout(function () {
    console.log("asyncEv after a while: " + scopeF.asyncEv);
}, 100);